const express = require('express');
const app = express();

app.options('/', function (req, res) {
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Headers', 'Content-Type');
    res.status(200).send();
});

app.get('/', function (req, res) {
    const re = /^(https?:)?(\/\/)?[\d\w.-]+\//gm;
    let result = req.query.username.replace(re, '');

    result = result.replace(/\/.+/, '');
    result = result.replace(/@/g, '');

    res.set('Access-Control-Allow-Origin', '*');
    res.send(`@${result}`);
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});